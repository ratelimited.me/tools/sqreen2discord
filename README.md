# Sqreen2Discord

This is a tool that converts webhooks from the [Sqreen](https://sqreen.com) security automation provider to the Discord format as embeds.

## How to use

Clone this repo and edit the `index.php` file's configuration options to your liking.

## How to customize

Customizing the data that is sent to Discord, such as embed field names, pre-defined fields, and embed color/styling can be done through the `templates/` folder. All you have to do is find the template you'd like to change and modify it.
